import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;


class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
      apiKey: "AIzaSyDte7NsdWLQXlqKNWn4i_dEVkNJzSwsTa8",
      authDomain: "flutre-f3577.firebaseapp.com",
      projectId: "flutre-f3577",
      storageBucket: "flutre-f3577.appspot.com",
      messagingSenderId: "491897381139",
      appId: "1:491897381139:web:545de69538cb2036befae6",
      measurementId: "G-X0HPHGCBFJ"
  );

  static const FirebaseOptions android = FirebaseOptions(
      apiKey: "AIzaSyDte7NsdWLQXlqKNWn4i_dEVkNJzSwsTa8",
      authDomain: "flutre-f3577.firebaseapp.com",
      projectId: "flutre-f3577",
      storageBucket: "flutre-f3577.appspot.com",
      messagingSenderId: "491897381139",
      appId: "1:491897381139:web:545de69538cb2036befae6",
      measurementId: "G-X0HPHGCBFJ"
  );

  static const FirebaseOptions ios = FirebaseOptions(
      apiKey: "AIzaSyDte7NsdWLQXlqKNWn4i_dEVkNJzSwsTa8",
      authDomain: "flutre-f3577.firebaseapp.com",
      projectId: "flutre-f3577",
      storageBucket: "flutre-f3577.appspot.com",
      messagingSenderId: "491897381139",
      appId: "1:491897381139:web:545de69538cb2036befae6",
      measurementId: "G-X0HPHGCBFJ"
  );

}
