import 'dart:js_interop';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

class FirebaseAuthService{

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User?> signUpUserWithEmailAndPassword(String email, String password)async {

    try{
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      return userCredential.user;
    }on FirebaseAuthException catch(e){
      print("firebase error");
    } catch(e) {
      print('Something went wrong');
    }
    return null;

  }
  /// thes fucntion is to login user with email and password
  Future<User?> loginInWithEmailAndPassword(String email,String password) async{
    try{
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      return userCredential.user;
    }on FirebaseAuthException catch(e){
      print("firebase auth exception $e");

    }

    catch(e){
      print("Something went wrong!!");
    }
  }
}